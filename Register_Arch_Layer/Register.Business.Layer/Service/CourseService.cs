﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using Register.Business.Layer.Dto;
using Register.Data.Layer.Models;
using Register.Data.Layer.Repositories;



namespace Register.Business.Layer.Service
{
    public class CourseService
    {
//TODO Wszystko co leci w dol do bazy
        


        public static bool AddCourse(CourseDto course)
        {
            var newCourse = Mappers.DtoToEntityMapper.CourseDtoToModelToEntity(course);
            Data.Layer.Repositories.CourseRepo.AddCourse(newCourse);
            return true;
        }

        public static bool CheckIfStudentExists(long pesel)
        {
            if (Data.Layer.Repositories.StudentRepo.CheckIfStudentExists(pesel) == false)
            {
                return false; //nie ma takiego peselu w bazie
            }
            return true; //jest taki pesel w bazie

        }

//TODO Wszystko co leci do gory

        public static StudentDto GetStudentFromDl(long pesel)
        {
            StudentRepo studentRepo = new StudentRepo();
            var student = studentRepo.GetStudentByPesel(pesel);
            return Register.Business.Layer.Mappers.EntityToDtoMapper.StudentEntityModelToDto(student);
        }

        public static bool CheckIfCourseExistss(int id)
        {
          var success =  Register.Data.Layer.Repositories.CourseRepo.CheckIfCourseExists(id);
            return success;
        } //sprawdza czy kurs istnieje true = isntieje


        public static List<StudentDto> GetStudentListFromDl(int id)
        {
            List<StudentDto> studentListFromCourse = null;
            // StudentRepo studentRepo = new StudentRepo();
            
             var StudentListFromDl = Register.Data.Layer.Repositories.CourseRepo.GetStudentsListFromCourse(id);

            studentListFromCourse = Register.Business.Layer.Mappers.EntityToDtoMapper.StudentListToStudentDtoList(StudentListFromDl);

            return studentListFromCourse;
        }//bierze liste uczestników kursu jeśli dostanie nazwe kursu


        public CourseDto courseDto(Course course)
        {
            CourseDto courseDto = new CourseDto();
            //mapuj
            
            return courseDto;
        }
    }

}
