﻿

using Register.Business.Layer.Dto;
using Register.Data.Layer.Models;
using Register.Data.Layer.Repositories;

namespace Register.Business.Layer.Service
{
    public class CourseDayService
    {
        
        //TODO Wszystko co leci w dol do bazy

        public static bool AddNewDay(CourseDayDto courseDayDto)
        {
            var courseDay = new CourseDay();
            courseDay = Register.Business.Layer.Mappers.DtoToEntityMapper.CourseDayDtoToModelEntity(courseDayDto);
            var  succes =  Register.Data.Layer.Repositories.CourseDayRepo.AddNewCourseDay(courseDay);
            if (succes)
            {
                return true;
            }
            return false;
        }




        public static bool UpdateCourseDayWithNewPresence(CourseDayDto courseDayDto)
        {
            var updateCourseDay = Mappers.DtoToEntityMapper.CourseDayDtoToModelEntity(courseDayDto);
            bool success = Register.Data.Layer.Repositories.CourseDayRepo.UpdateCourseDayFromSelectedIds(updateCourseDay);
            if (success)
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }




        //TODO Wszystko co leci do gory
        public static CourseDayDto GetCourseDayByIds (int idStudent, int idCourse)
        {
            var courseDayRepo = new CourseDay();
            courseDayRepo = Register.Data.Layer.Repositories.CourseDayRepo.GetCourseDayFromD(idStudent, idCourse);

            return Register.Business.Layer.Mappers.EntityToDtoMapper.CourseDayModelToDto(courseDayRepo);
        }
       


    }
        //TODO HELPER




    
}
