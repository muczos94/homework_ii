﻿using System.Net.NetworkInformation;
using System.Data;
using System.Runtime.Serialization.Formatters;

namespace Register.Business.Layer.Service
{
    public class StudentService
    {
        public static bool  AddStudents(StudentDto studentDto) //Przekaz do mappowania
        {


            //Sprawdz czy podany pesel jest w bazie danych?
            if (Data.Layer.Repositories.StudentRepo.CheckIfStudentExists(studentDto.Pesel) == false)
            {
                var person = Mappers.DtoToEntityMapper.StudentDtoModelToEntity(studentDto); //mapuje studenta do modelu dostepnego bazie danych
                Data.Layer.Repositories.StudentRepo.AddStudent(person); //dodaje studenta do bazy
                return false;
            }
            else
            {
                return true;
            }
        }

        public static bool CheckIfClientPeselExists(long pesel)
        {
            var checkPesel = Register.Business.Layer.Mappers.DtoToEntityMapper.PeselDtoToEntityModel(pesel);
            if (Register.Data.Layer.Repositories.StudentRepo.CheckIfPeselExists(checkPesel))
            {
                return true;//istnieje
            }
            else
            {
                return false;//nie istnieje
            }

        }

    }
}