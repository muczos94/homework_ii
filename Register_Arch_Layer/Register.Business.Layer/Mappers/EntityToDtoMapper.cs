﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using Register.Business.Layer.Dto;
using Register.Data.Layer.Models;


namespace Register.Business.Layer.Mappers
{
    internal class EntityToDtoMapper
    {
        public static StudentDto StudentEntityModelToDto(Student student)
        {
            if (student == null)
            {
                return null;
            }

            var studentDto = new StudentDto();

            studentDto.Id = student.Id;
            studentDto.Name = student.Name;
            studentDto.Surname = student.Surname;
            studentDto.Pesel = student.Pesel;
            studentDto.DateOfBirth = student.DateOfBirth;
            studentDto.Sex = student.Sex;
            

            return studentDto;
        }

        public static List<StudentDto> StudentListToStudentDtoList(List<Student> studentList)
        {
            List<StudentDto> studentListFomCourse = new List<StudentDto>();
            foreach (var studentD in studentList)
            {
                studentListFomCourse.Add(StudentEntityModelToDto(studentD));
            }

            return studentListFomCourse;
        }

        public static CourseDayDto CourseDayModelToDto(CourseDay courseDay)
        {
            if (courseDay == null)
            {
                return null;
            }
            CourseDayDto courseDayDto = new CourseDayDto();
            courseDayDto.Id = courseDay.Id;
            courseDayDto.Present = courseDay.Present;
            courseDayDto.Absent = courseDay.Absent;
            courseDayDto.Allpresence = courseDay.Allpresence;
            courseDayDto.Student.Id = courseDay.Student.Id;
            courseDayDto.Course.Id = courseDay.Course.Id;

            return courseDayDto;
        }

        public static CourseDto CourseModelToDto(Course course)
        {
            if (course == null)
            {
                return null;
            }
            CourseDto courseDto = new CourseDto();
            courseDto.Id = course.Id;
            courseDto.CourseTitle = course.CourseTitle;
            courseDto.DateStart = course.DateStart;
            courseDto.HomeworkThreshold = course.HomeworkThreshold;
            courseDto.NoStudent = course.NoStudent;
            courseDto.PresenceThreshold = course.PresenceThreshold;
             //bez listy studentow - czy zeby przypisac kurs do kursu  (CourseDay) musze mieć wyciągnietę również liste studentów 
             //bez listy dni z kursami

            return courseDto;
        }

     

    }
}
