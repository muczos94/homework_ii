﻿namespace Register.Business.Layer.Dto
{
    public class HomeworkDto
    {
        public int Id;
        public int CounterHomework;
        public int MaxPoints;
    }
}
