﻿

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Register.Data.Layer.Models
{
    public class CourseDay
    {
        public int Id { get; set; }
        public int Present { get; set; }
        public int Absent { get; set; }
        public int Allpresence { get; set; }

        public Course Course { get; set; } // bede mial id kursu w tabeli
        public Student Student { get; set; } // bede mial id studenta 

        
    }
}