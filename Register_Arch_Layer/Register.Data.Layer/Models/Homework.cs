﻿namespace Register.Data.Layer.Models
{
    public class Homework
    {
        public int Id { get; set; }
        public int DayNumber { get; set; }
        public int CounterHomework { get; set; }
    }
}