﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Register.Data.Layer.Models
{
    public class Student
    {
        public int Id { get; set; }
        public long Pesel { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime DateOfBirth { get; set; }
        public long Sex { get; set; }

        public List<Course> Courses { get; set; }

        public List<CourseDay> CourseDays { get; set; }
    }
}
