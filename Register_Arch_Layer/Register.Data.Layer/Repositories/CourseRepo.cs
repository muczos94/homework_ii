﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Register.Data.Layer.DbContexts;
using Register.Data.Layer.Models;

namespace Register.Data.Layer.Repositories
{
    public class CourseRepo
    {
        public static bool AddCourse(Course course)
        {
            var rowsAffected = 0;
            using (var dbContext = new RegisterDbContext())
            {
                List<Student> listOfStudents = new List<Student>();
                //myk, zamiana listy studentow na liste studentow wyciagnietych prosto z bazy
                foreach (var student in course.StudentList)
                {
                    Student studentFromData = new Student();
                    studentFromData = dbContext.StudentDbSet.First(p => p.Pesel == student.Pesel);
                    listOfStudents.Add(studentFromData);
                }
                course.StudentList = listOfStudents;//zamiana list

                dbContext.CourseDbSet.Add(course);
                rowsAffected = dbContext.SaveChanges();
            }
            return rowsAffected == 1;
        } //dodaj kurs

        public static bool CheckIfCourseExists(int Id)
        {
            using (var dbContext = new RegisterDbContext())
            {
                var checkCourseExists = dbContext.CourseDbSet.FirstOrDefault(x => x.Id == Id);
                dbContext.SaveChanges();
                if (checkCourseExists == null)
                    return false;//kurs nie istnieje w bazie
                return true;//kurs istnieje w bazie

            }
        } //sprawdz czy kurs istnieje

        public static List<Student> GetStudentsListFromCourse(int id)
        {
            List<Student> studentListFromCourse = null;
            using (var dbContext = new RegisterDbContext())
            {
              studentListFromCourse = dbContext.CourseDbSet.Include(x => x.StudentList).First(d=> d.Id == id).StudentList;
            }
            return studentListFromCourse;
        } //wez liste studentow z kursu

        public static Course GetCourse(int selectedCourse)
        {
            Course course = null;

            using (var dbContext = new RegisterDbContext())
            {
                dbContext.CourseDbSet.Include(a=> a.StudentList).First(x => x.Id == selectedCourse);
            }

            return course;
        }

      
        
    }
}