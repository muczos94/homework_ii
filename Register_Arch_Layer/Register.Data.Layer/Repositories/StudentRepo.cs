﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Register.Data.Layer.DbContexts;

using Register.Data.Layer.Models;

namespace Register.Data.Layer.Repositories
{
    public class StudentRepo
    {
        public Student GetStudentByPesel(long pesel)
        {
           Student studentfromDL = null;

            using (var dbContext = new RegisterDbContext())
            {
                studentfromDL = dbContext.StudentDbSet.First(p => p.Pesel == pesel) ;
            }

            return studentfromDL;

        }

        public static bool CheckIfStudentExists(long pesel)
        {
            using (var dbContext = new RegisterDbContext())
            {
            var checkPeselExists = dbContext.StudentDbSet.FirstOrDefault(x=> x.Pesel == pesel);
                dbContext.SaveChanges();
                if (checkPeselExists == null) 
                    return false;//pesel nie istnieje w bazie
                return true;//pesel istnieje w bazie

            }
           
        }

        public static bool CheckIfPeselExists(long pesel)
        {
            using (var dbContext = new RegisterDbContext())
            {
                var checkIfPeselExists = dbContext.StudentDbSet.Count(x => x.Pesel == pesel);
                if (checkIfPeselExists != 0)
                {
                    return true; //pesel istnieje juz w bazie
                }
                else
                {
                    return false; //pesel nie istnieje w bazie
                }
            }
        } //czy pesel istnieje w bazie klientow

        public static  bool AddStudent(Student student)
        {
            var rowsAffected = 0;
            using (var dbContext = new RegisterDbContext())
            {
                dbContext.StudentDbSet.Add(student);
                rowsAffected = dbContext.SaveChanges();


            }
            return rowsAffected == 1;
        }

    }
}
