﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Register.Data.Layer.DbContexts;
using Register.Data.Layer.Models;

namespace Register.Data.Layer.Repositories
{
    public class CourseDayRepo
    {

        public static CourseDay GetCourseDayFromD(int idStudent, int idCourse)
        {
            CourseDay courseDay = null;

            using (var dbContext = new RegisterDbContext())
            {
               // courseDay = dbContext.CourseDayDbSet.First(x => x.Course.Id == idCourse &&  x.Student.Id == idStudent);
                    courseDay =  dbContext.CourseDayDbSet
                    .Include(c => c.Course) //Dodaje obiekt klasy Course ktory jest w relacji z CourseDay
                    .Include(c => c.Student) //Dodaje obiekt klasy Student ktory jest w realcji z CourseDay
                    .Single(c => c.Course.Id == idCourse && c.Student.Id == idStudent); //wybiera dokladnie jeden obiekt, ktory ma id kursu i studenta zgodne z podanym jako parametry wywolania metody w ktorej jestesmy
                

            }

            return courseDay;
        }

        public static bool AddNewCourseDay(CourseDay courseDay)
        {
            using (var dbContext = new RegisterDbContext())
            {
                var courseDayFromD = dbContext.CourseDayDbSet.First();
                courseDayFromD.Id = courseDay.Id;
                courseDayFromD.Student.Id = courseDay.Student.Id;
                courseDayFromD.Course.Id = courseDay.Course.Id;
                courseDayFromD.Allpresence = courseDay.Allpresence;
                courseDayFromD.Absent = courseDay.Absent;
                courseDayFromD.Present = courseDay.Present;
                return true;
            }
        }
        
        public static bool UpdateCourseDayFromSelectedIds(CourseDay courseDay)
        {
            using (var dbContext = new RegisterDbContext())
            {
                var courseDayFromD =
                    dbContext.CourseDayDbSet.First(x => x.Course.Id == courseDay.Id && x.Student.Id == courseDay.Id);
             

                //Tutaj masz inaczej niz wyzej. Mianowicie tutaj:
                //1. Nie zaciagac rekordow ktore sa w relacji z Twoim rekordem (brak includeow jakichkolwiek)
                //2. Bierzesz pierwsze wystapienie (o gory bierzesz nie tylko pierwsze, ale i jedyne jakie jest w bazie) roznica miedzy First i Single => MSDN
                // courseDayFromD.Student.Id = courseDay.Student.Id;
                courseDayFromD.Absent = courseDay.Absent;
                courseDayFromD.Allpresence = courseDay.Allpresence;
              // courseDayFromD.Course.Id = courseDay.Course.Id; //wywal
                courseDayFromD.Id = courseDay.Id; //wywal
                courseDayFromD.Present = courseDay.Present;
                dbContext.CourseDayDbSet.Add(courseDayFromD);
                //no i chyba powinno byc spoko - ale jezeli chcesz robic upgrade ID dla studenta i course'u, powinienes zrobic include.
                //Inaczej dostniesz null reference exception
            
               
            return true;//kurs istnieje w bazie
            }
            
        }

        

    }
}
