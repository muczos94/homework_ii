﻿using System.Configuration;
using System.Data.Entity;
using Register.Data.Layer.Models;

namespace Register.Data.Layer.DbContexts
{
    internal class RegisterDbContext : DbContext
    {
        public RegisterDbContext() : base(GetConnectionString())
        { }

        public DbSet<Student> StudentDbSet { get; set; }
        public DbSet<Course> CourseDbSet { get; set; }
        public DbSet<CourseDay> CourseDayDbSet { get; set; }


        private static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["RegisterSqlDb"].ConnectionString;
        }

    }
}

