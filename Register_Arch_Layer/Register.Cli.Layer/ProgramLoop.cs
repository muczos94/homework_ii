﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Data.SqlTypes;
using Register.Cli.Layer.IoHelper;
using Register.Business.Layer;
using Register.Business.Layer.Dto;
using Register.Business.Layer.Service;


namespace Register.Cli.Layer
{
    public class ProgramLoop
    {
        public int SelectedCourse; // wybrany kurs
        public long SelectedPesel;
        public bool Succes;


        public enum CommandType
        {
            SelectCourse,
            AddNewCourse,
            AddNewStudent,
            AddNewDay,
            AddNewHomework,
            Exit
        }

        public void Execute()
        {
            
            var exit = false;

            while (!exit)
            {
                var command = ConsoleReadHelper.GetCommnadType();

                switch (command)
                {
                    case CommandType.SelectCourse:
                        SelectCourse();
                        break;
                    case CommandType.AddNewCourse:
                        AddNewCourse();
                        break;
                    case CommandType.AddNewStudent:
                        AddNewStudent();
                        break;
                    case CommandType.AddNewDay:
                        AddNewDayOfCourse();
                        break;
                    case CommandType.AddNewHomework:
                        break;
                    case CommandType.Exit:
                        exit = true;
                        break;
                    default:
                        Console.WriteLine("Program nie obsluguje ' " + command + " '");
                        break;

                }
            }
        }

        private bool CheckIfPeselExists()
        {
            var studentDto = new StudentDto();

            do
            {
                Succes = Register.Business.Layer.Service.StudentService
                    .CheckIfClientPeselExists(studentDto.Pesel = ConsoleReadHelper.GetLong("Podaj Pesel: "));
                if (Succes)
                {
                    Console.WriteLine("Wprowadzony pesel, istnieje juz w bazie danych..\n");
                }

            } while (Succes);
            SelectedPesel = studentDto.Pesel;

            return true;
        } //metoda do sprawdzania czy pesel istnieje w bazie

        private void AddNewCourse()
        {
            var courseDto = new CourseDto();
            courseDto.CourseTitle = ConsoleReadHelper.GetString("Tytul kursu: ");
            courseDto.Teacher = ConsoleReadHelper.GetString("Nazwa prowadzacego: ");
            courseDto.DateStart = ConsoleReadHelper.GetDate("Data rozpoczecia: ");
            courseDto.HomeworkThreshold = ConsoleReadHelper.GetInt("Prog dla prac domowych[%]: ");
            courseDto.PresenceThreshold = ConsoleReadHelper.GetInt("Prog dla obecnosci[%]: ");
            courseDto.NoStudent = ConsoleReadHelper.GetInt("Liczba Studentów: ");
            courseDto.StudentDtosList = new List<StudentDto>();

            var succes = false;
            int i=0;

            do { 
                try
                {
                    var pesel = ConsoleReadHelper.GetLong("Podaj pesel studenta: ");
                    var success = CourseService.CheckIfStudentExists(pesel);

                    if (success == true)
                    {
                        var students = Register.Business.Layer.Service.CourseService.GetStudentFromDl(pesel);
                        //student jest w bazie więc mozna go dodać 
                        courseDto.StudentDtosList.Add(students);
                        i++;
                    }
                    else
                    {
                        //wróć do pętli i zapytaj jeszcze raz
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("\nOsoba o podanym peselu, juz istnieje!!!\n");
                }
            } while (i<courseDto.NoStudent);
            //   StudentDtoListToStudentList(StudentDtosList);
            CourseService.AddCourse(courseDto);
            succes = true;
            ConsoleWriteHelper.PrintOperationSuccessMessage(succes);

        } //dodaj kurs
        
        private void AddNewStudent()
        {

            var studentDto = new StudentDto();
            CheckIfPeselExists(); //sprawdz czy podany pesel istnieje w bazie
            studentDto.Pesel = SelectedPesel;
            studentDto.Name = ConsoleReadHelper.GetString("Imie: ");
            studentDto.Surname = ConsoleReadHelper.GetString("Nazwisko: ");
            studentDto.DateOfBirth = ConsoleReadHelper.GetDate("Data urodzin dd/mm/yyyy: ");
            studentDto.Sex = ConsoleReadHelper.GetSex("Kobieta = 1 / Mezczyzna = 2: \n Plec: ");
            var ifStudentWasAdd = StudentService.AddStudents(studentDto);
            if (!ifStudentWasAdd)
            {
                Console.Write("Dodano Studenta do bazy.\n\n");
            }
            else
            {
                Console.Write("Nie dadano studenta ponieważ ta osoba istnieje juz w bazie.\n\n");
            }
            
            
        } //dodaj studenta

        private int SelectCourse()
        {
            bool exists;
           
            do
            {
                try
                {
                   SelectedCourse = ConsoleReadHelper.GetInt("Podaj Id kursu który na ktorym chcesz pracowac:  ");
                   exists = Business.Layer.Service.CourseService.CheckIfCourseExistss(SelectedCourse); //jesli kurs istnieje to "true"
                    if (exists)
                    {
                        Console.WriteLine("Wybrano kurs o indeksie: " + SelectedCourse);
                    }
                    else
                    {
                        Console.WriteLine("Podany kurs - nie istnieje.\n");
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("--Nie ma takiego kursu!\n");
                    throw;
                }
 
            } while (!exists);

            return SelectedCourse;
        } //wybierz kurs 

        private void AddNewDayOfCourse()
        {
            while (SelectedCourse == 0) //sprawdzam czy zostal wybrany kurs
            {
                Console.WriteLine("Nie wybrales kursu dla ktorego chcesz dodac dzien!\n");
                SelectedCourse = SelectCourse();
            }
   
            {
                Console.Write("LISTA OBECNOSCI KURS: " + SelectedCourse + "\n\n");
                List<StudentDto> studentListFromCourse = new List<StudentDto>();
                studentListFromCourse =  Register.Business.Layer.Service.CourseService.GetStudentListFromDl(SelectedCourse);

                foreach (var student in studentListFromCourse)
                {
//TODO sprawdz czy to jest pierwsze wywolanie funkcji, jesli tak dodaj, jesli nie, uzyj update JACOB!

                    CourseDayDto courseDayDto = new CourseDayDto();
                     //courseDayDto.Course = SelectedCourse;
                     //courseDayDto.Student.Id = student.Id;
                    

                    Console.Write("1 = obecny / 0 i każda inna wartosc = nieobecny\n");
                    int answer = ConsoleReadHelper.GetInt("Czy " + student.Name + " " + student.Surname +
                                                          " jest obecny?: ");

                    if (answer == 1)
                    {
                        courseDayDto.Present++;
                        courseDayDto.Allpresence++;
                        courseDayDto.Absent = 0;
                    }
                    else
                    {
                        courseDayDto.Absent++;
                        courseDayDto.Allpresence++;
                        courseDayDto.Present = 0;
                    }

                                            

                    //TODO ponizej znajduje sie funkcja do update obecnsoci po id kursu oraz studenta - jednak jest bezuzyteczna, jesli rekord jest pusty
                    //var courseDayDto = Register.Business.Layer.Service.CourseDayService.GetCourseDayByIds(student.Id, SelectedCourse);

                    //Console.Write("1 = obecny / 0 i każda inna wartosc = nieobecny\n");

                    //int answer  = ConsoleReadHelper.GetInt("Czy " + student.Name + " " + student.Surname + " jest obecny?");
                    //if (answer == 1)
                    //{
                    //    courseDayDto.Present++;
                    //    courseDayDto.Allpresence++;
                    //}
                    //else
                    //{
                    //    courseDayDto.Absent++;
                    //    courseDayDto.Allpresence++;
                    //}

                    //var succes = Register.Business.Layer.Service.CourseDayService.UpdateCourseDayWithNewPresence(courseDayDto); //try

                    //if (succes)
                    //{
                    //    Console.Write("update się udal!");
                    //}
                    //else
                    //{
                    //    Console.Write("update się NIE udal!");

                    //}


                }
            }
         //metoda ktora wyciagnie ten kurs
        } //dodaj prace domowa

     //  private void AddNewHomeworks() // in progress

     //  private void PrintRaport() // in progress






    }
}
