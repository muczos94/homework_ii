﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Register.Cli.Layer.IoHelper
{
    class ConsoleWriteHelper
    {
       

        public static void PrintOperationSuccessMessage(bool success)
        {
            if (success)
            {
                Console.WriteLine("Operacja powiodła sie!");
            }
            else
            {
                Console.WriteLine("Cos poszlo nie tak..");
            }
        }
    }
}
