﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Register.Cli.Layer.IoHelper
{
    public class ConsoleReadHelper
    {

        public static ProgramLoop.CommandType GetCommnadType()
        {
            ProgramLoop.CommandType commandType;
            Console.WriteLine("|------------------DZIENNIK ZAJEĆ------------------|\n" +
                              "\t\t\t\tmade by Jakub Gerlee \n" +
                              "\nSelectCourse - Wybierz kurs" +
                              "\nAddNewCourse - stworz kurs" +
                              "\nAddNewStudent - Dodanie nowego studenta" +
                              "\nAddNewDay - Dodanie dnia kursu" +
                              "\nAddNewHomework - Dodanie nowej pracy domowej" +
                              "\nPrintRaport - drukowanie raportu" +
                              "\nExit - Wyjscie z aplikacji\n\n");


            while (!Enum.TryParse(Console.ReadLine(), out commandType))
            {
                Console.WriteLine("Wprowadzono nieprawidlowy znak, spróbuj ponownie...\n");

                Console.WriteLine("|------------------DZIENNIK ZAJEĆ------------------|\n" +
                                  "\t\t\t\tmade by Jakub Gerlee \n" +
                                  "\nSelectCourse - Wybierz kurs" +
                                  "\nAddNewCourse - stworz kurs" +
                                  "\nAddNewStudent - Dodanie nowego studenta" +
                                  "\nAddNewDay - Dodanie dnia kursu" +
                                  "\nAddNewHomework - Dodanie nowej pracy domowej" +
                                  "\nPrintRaport - drukowanie raportu" +
                                  "\nExit - Wyjscie z aplikacji\n\n");
            }

            return commandType;
        } //Pobiera komende od uzytkownika

        public static ProgramLoop.CommandType GetCommandType(string message)
        {
            ProgramLoop.CommandType commandType;
            Console.Write(message + " (AddNewCourse" +
                          "AddNewStudent" +
                          "SelectCourse" +
                          "AddNewDay" +
                          "Exit)");

            while (!Enum.TryParse(Console.ReadLine(), out commandType))
            {
                Console.WriteLine("Niepoprawna komenda, spróbuj ponownie: \n");
            }

            return commandType;
        }


        public static int GetInt(string message)
        {
            int number;
            Console.Write(message);

            while (!Int32.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine("Podana liczba jest niepoprawna...\n");
                Console.Write("Wpisz poprawna komende: ");
            }

            return number;
        } //Parsuje na int

        public static long GetLong(string message)
        {
            long number;
            Console.Write(message);

            while (!long.TryParse(Console.ReadLine(), out number))
            {
                Console.Write("Podana liczba jest nieprawidlowego typu, sprobuj ponownie: ");
            }

            return number;
        } //Parsuje na long

        public static bool IsAllLetters(string value)
        {
                foreach (char c in value)
                {
                    if (!char.IsLetter(c))
                        return false;
                }
            return true;
        }

        public static DateTime GetDate(string message)
        {
            DateTime dateTime;
            Console.Write(message);

            while (!DateTime.TryParse(Console.ReadLine(), out dateTime))
            {
                Console.Write("Podales niepoprawna wartosc..\n\n");
                Console.Write(message);
            }

            return dateTime;
        } //Pobiera DateTime w prawidłowym formacie

        public static string GetString(string message) //Sprawdza czy podany typ jest String
        {
 
            while (true)
            {
                Console.Write(message);
                var variable = Console.ReadLine();
                while(String.IsNullOrEmpty(variable))
                {
                    Console.Write("Nic nie wpisales..\n\n");
                    Console.Write(message);
                    

                    variable = Console.ReadLine();
                }
                return variable;

            }
        }

        public static int GetSex(string message)
        {
            Console.Write(message);
            while (true)
            {

                int number;
                while (!Int32.TryParse(Console.ReadLine(), out number))
                {
                    if (number == 1 || number == 2 )
                    {
                        return number;
                    }

                    Console.WriteLine("Podana liczba jest niepoprawna...\n");
                    Console.Write(message);

                }

                return number;

            }
        }


        //public static ProgramLoop.WhichSex GetWhichSex(string message)
        //{
        //    ProgramLoop.WhichSex whichSex;
        //    Console.Write(message + " (K - Kobieta/" +
        //                  "M - /Mezczyzna");

        //    while (!Enum.TryParse(Console.ReadLine(), out whichSex))
        //    {
        //        Console.WriteLine("Niepoprawna komenda, spróbuj ponownie...\n");
        //    }

        //    return whichSex;

        //}

    } 

    
}
